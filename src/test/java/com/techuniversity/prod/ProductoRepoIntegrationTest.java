package com.techuniversity.prod;

import com.techuniversity.prod.controllers.ProductoController;
import com.techuniversity.prod.productos.ProductoModel;
import com.techuniversity.prod.productos.ProductoRepository;
import org.junit.jupiter.api.Test;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;

import static org.junit.jupiter.api.Assertions.assertTrue;


import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;

import java.util.List;

@SpringBootTest
public class ProductoRepoIntegrationTest {
    @Autowired
    ProductoRepository productoRepository;

    @Test
    public void testFindAll() {
        List<ProductoModel> productos = productoRepository.findAll();
        assertTrue(productos.size() > 0);
    }

    @LocalServerPort
    private int port;
    TestRestTemplate testRestTemplate = new TestRestTemplate();
    HttpHeaders headers = new HttpHeaders();

    @Test
    public void testPrimerProducto() throws Exception {
        HttpEntity<String> entity = new HttpEntity<String>(null, headers);
        ResponseEntity<String> response = testRestTemplate.exchange(
                crearUrlConPuerto("/productos/productos/60acaa02c76ca21bcba82270"),
                HttpMethod.GET, entity, String.class
        );
        String expected = "{\"id\":\"08bcab58b43ab74accb25499\"," + "\"nombre\":\"Producto 1\",\"descripcion\":\"Descripción producto 1\"," + "\"precio\": 11.5}";
        JSONAssert.assertEquals(expected, response.getBody(), false);

    }
    private String crearUrlConPuerto(String url) {
        return "http://localhost:" + port + url;
    }

}
